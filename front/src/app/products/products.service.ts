import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { Product } from './product.class';

@Injectable({
  providedIn: 'root'
})
export class ProductsService {

    private static productslist: Product[] = null;
    private products$: BehaviorSubject<Product[]> = new BehaviorSubject<Product[]>([]);
    private apiToken = 'hVr8WTeT2V2raxd8zc78iZoARozbyXtKjyooyJzU0i6jaZhCgPzxHkJ9fOEtzrJj';
    private headers = {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${this.apiToken}`
    };

    constructor(private http: HttpClient) { }

    getProducts(): Observable<Product[]> {

        if( ! ProductsService.productslist )
        {
            this.http.get<any>('http://localhost:4000/api/products',{
                headers: this.headers
            }).subscribe(
                data => {                
                    ProductsService.productslist = data;
                    
                    this.products$.next(ProductsService.productslist);
                },
                error => {
                    console.log(error.error);
                }
            );
        }
        else
        {
            this.products$.next(ProductsService.productslist);
        }

        return this.products$;
    }

    create(prod: Product): Observable<Product[]> {

        this.http.post<any>('http://localhost:4000/api/products',{
            name : prod.name,
            category : prod.category,
            code : prod.code,
            description : prod.description,
            image : prod.image,
            inventoryStatus : prod.inventoryStatus,
            price : prod.price,
            quantity : prod.quantity
        },{headers: this.headers})
        .subscribe(
            response => {
                ProductsService.productslist.push(prod);
                this.products$.next(ProductsService.productslist);
            },
            error => {
                console.log(error.error);
            }
        );
        
        return this.products$;
    }

    update(prod: Product): Observable<Product[]>{

        this.http.patch<any>(`http://localhost:4000/api/products/${prod.id}`,{
            name : prod.name,
            category : prod.category,
            code : prod.code,
            description : prod.description,
            image : prod.image,
            inventoryStatus : prod.inventoryStatus,
            price : prod.price,
            quantity : prod.quantity
        },{headers: this.headers})
        .subscribe(
            response => {
                ProductsService.productslist.forEach(element => {
                    if(element.id == prod.id)
                    {
                        element.name = prod.name;
                        element.category = prod.category;
                        element.code = prod.code;
                        element.description = prod.description;
                        element.image = prod.image;
                        element.inventoryStatus = prod.inventoryStatus;
                        element.price = prod.price;
                        element.quantity = prod.quantity;
                        element.rating = prod.rating;
                    }
                });
                this.products$.next(ProductsService.productslist);
            },
            error => {
                console.log(error.error);
            }
        );
 
        return this.products$;
    }


    delete(id: number): Observable<Product[]>{

        this.http.delete<any>(`http://localhost:4000/api/products/${id}`,{
            headers: this.headers
        }).subscribe(
            data => {
                console.log(data);
                ProductsService.productslist = ProductsService.productslist.filter(value => { return value.id !== id } );
                this.products$.next(ProductsService.productslist);
                
            },
            error => {
                console.log(error.error);
            }
        )
        return this.products$;
        
    }
}