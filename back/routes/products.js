const express = require('express');
const ROUTER = express.Router();

const { getProducts, createProducts, getProductById, updateProduct, deleteProduct } = require('../controllers/productsController');
//get all products
ROUTER.get('/', getProducts);
//Create a product
ROUTER.post('/', createProducts);
//Get a product by id
ROUTER.get('/:productId', getProductById);
//Update a product
ROUTER.patch('/:productId', updateProduct);
//Delete a product
ROUTER.delete('/:productId', deleteProduct);

module.exports = ROUTER;