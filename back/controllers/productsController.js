const { client } = require('../utils/db');
const Products = require('../models/products');
const checkNumber = ["price","quantity","rating"];

/**
 * Retrieve all products   
 * @param {*} req Request
 * @param {*} res Response
 * @returns Products
 */
const getProducts = async (req,res) => {
    client(req,res);

    Products.find().then(products => {
        return res.status(200).json(products)
    })
    .catch(err => {
        return res.status(500).json(err);
    });
}

/**
 * Create a new products
 * @param {*} req Request
 * @param {*} res Response
 * @returns Message
 */
const createProducts = async (req,res) => {
    client(req,res);
    
    const bodyData = req.body;

    for(const propertie in bodyData){
        if(!bodyData[propertie]){
            return res.status(400).json("A product properties is empty");
        }

        if(checkNumber.includes(propertie)){
            if(isNaN(bodyData[propertie])){
                return res.status(400).json("A product properties is wrong");
            }
        }
    }

    bodyData["id"] = await Products.countDocuments() + 1000;
    bodyData["rating"] = 3;
    
    Products.create(bodyData)
    .then((product) => {
        return res.status(201).json({
            message: "Product created !",
            data: product
        })
    })
    .catch(err => {
        return res.status(500).json(err);
    });
}

/**
 * Retrieve details for specific product
 * @param {*} req Request
 * @param {*} res Response
 * @returns Product
 */
const getProductById = (req,res) => {
    client(req,res);

    if(!req.params.productId || isNaN(req.params.productId)) return res.status(200).json("Product id is incorrect !");
    const id = Number(req.params.productId);

    Products.findOne({id:id}).then(product => {
        if(!product) return res.status(200).json("Product not found !")
        return res.status(200).json(product)
    })
    .catch(err => {
        return res.status(500).json(err);
    });
}

/**
 * Update details of product if it exists 
 * @param {*} req Request
 * @param {*} res Response
 * @returns 
 */
const updateProduct = async (req,res) => {
    client(req,res);

    if(!req.params.productId || isNaN(req.params.productId)) return res.status(200).json("Product id is incorrect !");
    const id = Number(req.params.productId);

    if(!await Products.findOne({id:id})) return res.status(200).json(`Product ${id} not found`);

    const bodyData = req.body;

    for(const propertie in bodyData){
        if(!bodyData[propertie]){
            return res.status(400).json("A product properties is empty");
        }

        if(checkNumber.includes(propertie)){
            if(isNaN(bodyData[propertie])){
                return res.status(400).json("A product properties is wrong");
            }
        }
    }

    Products.findOneAndUpdate({id:id},bodyData)
    .then((product) => {
        return res.status(201).json({
            message: `Product ${req.params.productId} updated`,
            data: product
        })
    })
    .catch(err => {
        return res.status(500).json(err);
    });
}

/**
 * Remove product
 * @param {*} req Request
 * @param {*} res Response
 * @returns 
 */
const deleteProduct = async (req,res) => {
    client(req,res);

    if(!req.params.productId || isNaN(req.params.productId)) return res.status(200).json("Product id is incorrect !");
    const id = Number(req.params.productId);

    if(!await Products.findOne({id:id})) return res.status(200).json(`Product ${id} not found`);

    Products.findOneAndDelete({id:id})
    .then(() => {
        return res.status(201).json(`Product ${req.params.productId} deleted`)
    })
    .catch(err => {
        return res.status(500).json(err);
    });
}

module.exports = {
    getProducts,
    createProducts,
    getProductById,
    updateProduct,
    deleteProduct
}