const { model, Schema } = require('mongoose');
const Products = new Schema({
    id: Number,
    code: String,
    name: String,
    description: String,
    price: Number,
    quantity: Number,
    inventoryStatus: String,
    category: String,
    image: String,
    rating: Number
});

module.exports = model('products',Products);
  