const Mongoose = require('mongoose');
const mongodbURL = process.env.DB_TOKEN;

/**
 * Create a mongoDB connection
 * @param {*} req Request
 * @param {*} res Response
 * @returns 
 */
const client = async (req,res) => {
    if(!mongodbURL) return res.status(503).send("Service Unavailable");

    Mongoose.connect(mongodbURL || '', {
        dbName:'AltenShop'
    });

    if(!Mongoose.connect) return res.status(500).send("Internal Server Error");
}

module.exports = {
    client
}