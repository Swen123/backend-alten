require("dotenv").config();
const port = process.env.API_PORT || 3000;

const express = require('express');
const cors = require('cors');
const app = express();
const productsRoutes = require('./routes/products');

app.use(cors())
app.use(express.json());

app.use((req,res, next) => {
    const auths = req.headers.authorization.split(" ");
    const authType = auths[0];
    const token = auths[1];

    if(req.headers.authorization && authType === "Bearer"){
        if(token !== process.env.AUTH_TOKEN){
            return res.status(401).send('Unauthorized connection !');
        }
        next();
    }
});

app.use('/api/products',productsRoutes);

app.listen(port, () => {
    console.log(`Api is listening on http://localhost:${port}`);
});