# Back-end

Développer un back-end permettant la gestion de produits définis plus bas. Vous pouvez utiliser la technologie de votre choix parmis la list suivante :

- nodejs/express
- Java/Spring Boot
- C#/.net Core
- Python/Flask

Le back-end doit gérer les API REST suivantes : 

| Resource           | POST                  | GET                            | PATCH                                    | PUT | DELETE           |
| ------------------ | --------------------- | ------------------------------ | ---------------------------------------- | --- | ---------------- |
| **/products**      | Create a new products | Retrieve all products          | X                                        | X   |     X            |
| **/products/1**    | X                     | Retrieve details for product 1 | Update details of product 1 if it exists | X   | Remove product 1 |

Un produit a les caractéristiques suivantes : 

``` typescript
class Product {
  id: number;
  code: string;
  name: string;
  description: string;
  price: number;
  quantity: number;
  inventoryStatus: string;
  category: string;
  image?: string;
  rating?: number;
}
```

Le back-end créé doit pouvoir gérer les produits dans une base de données SQL/NoSQL ou dans un fichier json.

Une liste de produits est disponible dans ce fichier : `front/assets/products.json`

Un front-end en Angular est disponible et permet d'utiliser l'API via cette adresse : `http://localhost:3000`

vous pouvez lancer le front-end angular avec la commande 'ng serve'

# Bonus

Vous pouvez ajouter des tests Postman ou Swagger pour valider votre API


# Configuration de l'API

Installation des packages JavaScript

```bash
  cd back
  npm install
  node index.js
```
L'API tourne sur le pourt 4000 par défaut, le fichier .env permet de changer cette configuration, si aucun port n'est précisé alors le port 3000 sera utilisé.

La base de données utilisée est mongoDB, donc pas besoin de configurer ou importer la base de données en local.

## Postman

Un fichier est disponible dans le répertoire "back/Alten-shop.postman_collection.json" qui doit être importé dans postman

Il regroupe toutes les routes ainsi que les tests des différentes routes.





